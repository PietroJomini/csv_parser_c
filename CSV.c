/*
    Tiny and non-optimized lib for parsing CSV files in c.
    By: Pietro Jomini
*/

#include "./CSV.h"

CSV_tokenized_line CSV_tokenize_line (char *line, char *separator, unsigned int max_rows) {

    CSV_tokenized_line tk_line;
    tk_line.rows_count = 0;

    // Allocating mem for the array of string.
    tk_line.tokens = (char **)malloc(sizeof(char *) * max_rows);

    // Getting the first substring, from line[0] the the first occurrence of the separator.
    char *token = strtok(line, separator);

    // Iterating through the string, each time checking it's end.
    while (tk_line.rows_count < max_rows && token != NULL) {

        // Coping the substring (token) int the tk_like struct.
        tk_line.tokens[tk_line.rows_count] = (char *)malloc(sizeof(char)*(strlen(token) + 1));
        strcpy(tk_line.tokens[tk_line.rows_count], token);

        tk_line.rows_count += 1;

        // Getting the next substring
        token = strtok(NULL, separator);
    }

    return tk_line;
}

CSV_tokenized_line CSV_get_header (FILE *csv, char *separator, unsigned int max_lines_lenght, unsigned int max_rows) {

    // Going to the start of the file
    fseek(csv, 0, SEEK_SET);

    // Getting the first line of the file, 
    // checking it's existance 
    // and tokenizing her.
    CSV_tokenized_line header;
    header.rows_count = 0;
    char line[max_lines_lenght];
    if (fgets(line, max_lines_lenght, csv) != NULL) header = CSV_tokenize_line(line, separator, max_rows);

    return header;
}

unsigned int CSV_parse (FILE *csv, void *str_ptr, unsigned int str_size,  char *separator, unsigned int max_lines, unsigned int max_rows, unsigned int max_lines_lenght, void (*f)(void *, CSV_tokenized_line)) {
    
    // Pointer used to navigate through the array.
    void *iterator = str_ptr;

    // End of the segment allocated for the array.
    void *segment_end = str_ptr + str_size * max_lines;

    char line[max_lines_lenght];

    // Iterating through each line of the file.
    while (iterator < segment_end && fgets(line, max_lines_lenght, csv) != NULL) {

        // Removing the new line char from the line.
        line[strlen(line) - 1] = '\0';

        // Tokenizing the line.
        CSV_tokenized_line tk_line = CSV_tokenize_line(line, separator, max_rows);

        // Calling the "translator" function.
        (*f)(iterator, tk_line);

        // Cleaning the allocated memory
        CSV_free_parsed_row(&tk_line);

        // Moving to the next location.
        iterator += str_size;
    }

    // Returning the effective length of the array.
    return (iterator - str_ptr) / str_size;
}

void CSV_free_parsed_row (CSV_tokenized_line *line) {
    unsigned int i;

    // Iterating through tokens and freeing all of thems
    for (i = 0; i < line->rows_count; i++) 
        free(line->tokens[i]);
    
    // Freeing the container
    free(line->tokens);

    return;
}