/*
    Tiny and non-optimized lib for parsing CSV files in c.
    By: Pietro Jomini
*/

#ifndef __CSVC__
#define __CSVC__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
    Store a tokenized string, with **tokens as an array of strings
    and rows_count as number of strings in the array.
*/
typedef struct _CSV_tokenized_line {
    unsigned long int rows_count;
    char **tokens;
} CSV_tokenized_line;

/*
    CSV_tokenize_line: tokenize a line, 
    given a certain separator, and return a CSV_tokenized_line.
*/
CSV_tokenized_line CSV_tokenize_line (char *line, char *separator, unsigned int max_rows);

/*
    CSV_get_header: get the header of the file,
    by previously going to its the start.
    Return a CSV_tokenized_line.
*/
CSV_tokenized_line CSV_get_header (FILE *csv, char *separator, unsigned int max_lines_lenght, unsigned int max_rows) ;

/*
    CSV_parse: parse a CSV.
    Some parameters may need ulterior explanation:
        str_ptr: pointer the array of structure in which the CSV must be stored,
        str_size: size of the struct making the array,
        f: void function, used to "translate" a tokenized string into a struct.
    Return the effective size of the array.
*/
unsigned int CSV_parse (FILE *csv, void *str_ptr, unsigned int str_size,  char *separator, unsigned int max_lines, unsigned int max_rows, unsigned int max_lines_lenght, void (*f)(void *, CSV_tokenized_line));

/*
    Free allocated memory for the tokens
*/
void CSV_free_parsed_row (CSV_tokenized_line *line);

#include "./CSV.c"
#endif // !__CSVC_