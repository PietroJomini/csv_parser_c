# Tiny and non-optimized lib for parsing CSV files in c.
> By: Pietro Jomini :shipit:

## TODO:
- change strtok to a thread safe version (strtok_r)
- check that the last char is a \n before deleting it
- add multiple controls on user input

> [Usage example](https://github.com/PietroJominiITIS/TPSIT/tree/master/CSV_C)
